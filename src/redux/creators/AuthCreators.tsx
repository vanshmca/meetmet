import {
  AuthActionType,
  SET_USER_NUMBER,
  SET_USER_LOCATION,
  SET_USER_INFO,
  CLEAR_ALL,
  GET_TOKEN,
  GET_CONTACTS,
  GET_CATEGORIES,
  GET_FAV_CONTACTS
} from '../types';
import { Log } from '../../utils/Logger';

export function setUserNumber(userNumber: number): AuthActionType {
  return {
    type: SET_USER_NUMBER,
    payload: {
      userNumber: userNumber,
    },
  };
}
export function storeUserDetails(userInfo: Object): AuthActionType {
  return {
    type: SET_USER_INFO,
    payload: {
      preference: userInfo,
    },
  };
}

export function storeUserCurrentLocation(location: Object) {
  return {
    type: SET_USER_LOCATION,
    payload: location,
  };
}
export function setAccessToken(token: string): AuthActionType {
  Log(token, 'token');
  return {
    type: GET_TOKEN,
    payload: token,
  };
}
export function clearAll(): AuthActionType {
  return {
    type: CLEAR_ALL,
    payload: {
      token: undefined,
    },
  };
}
export function storeContacts(userInfo) {
  return {
    type: GET_CONTACTS,
    payload: {
      userInfo
    },
  };
}

export function storeCategories(Categories) {
  return {
    type: GET_CATEGORIES,
    payload: {
      Categories
    },
  };
}

export function storeFavContacts(userInfo) {
  return {
    type: GET_FAV_CONTACTS,
    payload: {
      userInfo
    },
  };
}
