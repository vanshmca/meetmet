import {
  AuthActionType,
  CLEAR_ALL,
  SET_USER_NUMBER,
  SET_USER_INFO,
  GET_TOKEN,
  UPDATE_PROFILE,
  GET_CONTACTS,
  GET_CATEGORIES,
  GET_FAV_CONTACTS
} from '../types';
import { User } from '../../types/modals/User';
import { Log } from '../../utils/Logger';

const INITIAL_STATE: User = {
  token: undefined,
  preference: undefined,
  userNumber: undefined,
  contacts: [],
  fcmToken: undefined,
  categories: [],
  favoritesContacts: []
};
export default (state = INITIAL_STATE, action: AuthActionType) => {
  switch (action.type) {
    case CLEAR_ALL:
      return { ...state, token: undefined };
    // return INITIAL_STATE;
    case SET_USER_NUMBER:
      return { ...state, userNumber: action.payload.userNumber };
    case SET_USER_INFO:
      return { ...state, preference: action.payload };
    case GET_TOKEN:
      return { ...state, token: action.payload };
    case GET_CONTACTS:
      return { ...state, contacts: action.payload };
    case GET_CATEGORIES:
      return { ...state, categories: action.payload };
    case UPDATE_PROFILE:
      return {
        ...state,
        preference: { ...action.payload },
      };
    case GET_FAV_CONTACTS:
      return { ...state, favoritesContacts: action.payload };
    default:
      return state;
  }
};
