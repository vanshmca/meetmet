import { Dispatch } from 'redux';
import { setLoading } from '../creators/LoadingCreator';
import {
  storeUserDetails,
  setAccessToken,
  clearAll,
  storeContacts,
  storeFavContacts
} from '../creators/AuthCreators';
import { AppActionTypes } from '../types';
import {
  executePostRequest,
  executeGetRequest,
  executePutRequest,
  executeDeleteRequest,
} from '../../utils/FetchUtils';
import {
  contact,
  Editcontact,
  Deletecontact,
  User
} from '../../types/modals/User';
import { RootState } from '../reducers';
import { Platform } from 'react-native';
import { Log } from '../../utils/Logger';
import { showAlert } from '../../utils/AlertHelper'
import { validateAddContacts } from '../../utils/ValidationHelper'

export const socialLoginAction = (uid: string, provider: string, name: string, email: string) => {
  return async (dispatch: Dispatch<AppActionTypes>) => {
    dispatch(setLoading(true));
    let loginResult: any
    try {
      loginResult = await executePostRequest(
        'v1/login',
        getSocialLoginFormData(uid, provider, name, email),
      );
      if (loginResult.response?.status == 200) {
        // if (token) {
        //   fetch('https://graph.facebook.com/v2.5/me?fields=email,name,first_name,middle_name,last_name,gender,address,picture.type(large)&access_token=' + token)
        //     .then((response) => {
        //       response.json().then((json) => {
        //         dispatch(storeUserDetails({ ...json, provider: provider }))
        //       })
        //     })
        //     .catch(() => {
        //       console.log('ERROR GETTING DATA FROM FACEBOOK')
        //     })
        // }
        dispatch(setAccessToken(loginResult.response.token))

      }
      dispatch(setLoading(false));
      return loginResult

    } catch (error) {
      showAlert(error)
      return loginResult

    }



  };
};

/**
 * API for login
 * @param data email,password
 * @returns 
 */
export const Login = (data: any) => {
  console.log("data--->", data);

  return async (dispatch: Dispatch<AppActionTypes>, getState: () => RootState,) => {
    const token = getState().persistedReducer.token;
    try {
      dispatch(setLoading(true));
      const result: any = await executePostRequest(
        'api/user/login',
        JSON.stringify(data),
      );

      dispatch(setLoading(false));
      console.log(result, "contacts")
      return result

    } catch (err) {
      dispatch(setLoading(false));
      console.log("SignUp Error===>", err);
    }

  };
};


/**
 * API for create new Account
 * @param data name:{firstName,lastName},email,password
 * @returns 
 */
export const SignUpAction = (data: any) => {
  console.log("data--->", data);

  return async (dispatch: Dispatch<AppActionTypes>, getState: () => RootState,) => {
    const token = getState().persistedReducer.token;
    try {
      dispatch(setLoading(true));
      const result: any = await executePostRequest(
        'api/user/signup',
        JSON.stringify(data),
      );

      dispatch(setLoading(false));
      console.log(result, "contacts")
      return result

    } catch (err) {
      dispatch(setLoading(false));
      console.log("SignUp Error===>", err);
    }

  };
};



