import BackBtton from './Images/ic_backButton.svg';
import EmailIcon from './Images/ic_email.svg';
import GoogleIcon from './Images/ic_google.svg';
import AppleIcon from './Images/ic_apple.svg';
import FacebookIcon from './Images/ic_facebook.svg';
import BackBlueIcon from './Images/ic_BlueBackIcon.svg'
import PhoneIcon from './Images/ic_phoneIcon.svg'
import EyeIcon from './Images/ic_eye.svg'
import EyeIconClose from './Images/ic_eyeClose.svg'
import CloseIcon from './Images/ic_close.svg'


export {
    BackBtton,
    EmailIcon,
    GoogleIcon,
    AppleIcon,
    FacebookIcon,
    BackBlueIcon,
    PhoneIcon,
    EyeIcon,
    EyeIconClose,
    CloseIcon,
};
