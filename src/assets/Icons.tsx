import React from 'react';
import {
  BackBtton,
  EmailIcon,
  GoogleIcon,
  AppleIcon,
  FacebookIcon,
  BackBlueIcon,
  PhoneIcon,
  EyeIcon,
  EyeIconClose,
  CloseIcon
} from './';

export enum Icons {
  BACK_BUTTON,
  EMAIL_ICON,
  GOOGLE_ICON,
  APPLE_ICON,
  FACEBOOK_ICON,
  BACK_BLUE_BUTTON,
  PHONE_ICON,
  EYE_ICON,
  EYE_ICON_CLOSE,
  CLOSE_ICON,
}

export const getIcons = (type: Icons, size?: number, colors?: string) => {
  const iconSize = size ? size : 20;
  const color = colors ? colors : "#DCE0E2"
  switch (type) {
    case Icons.BACK_BUTTON:
      return <BackBtton height={iconSize} width={iconSize} />;
    case Icons.EMAIL_ICON:
      return <EmailIcon height={iconSize} width={iconSize} />;
    case Icons.GOOGLE_ICON:
      return <GoogleIcon height={iconSize} width={iconSize} />;
    case Icons.APPLE_ICON:
      return <AppleIcon height={iconSize} width={iconSize} />;
    case Icons.FACEBOOK_ICON:
      return <FacebookIcon height={iconSize} width={iconSize} />;
    case Icons.BACK_BLUE_BUTTON:
      return <BackBlueIcon height={iconSize} width={iconSize} />;
    case Icons.PHONE_ICON:
      return <PhoneIcon height={iconSize} width={iconSize} />;
    case Icons.EYE_ICON:
      return <EyeIcon height={iconSize} width={iconSize} />;
    case Icons.EYE_ICON_CLOSE:
      return <EyeIconClose height={iconSize} width={iconSize} />;
    case Icons.CLOSE_ICON:
      return <CloseIcon height={iconSize} width={iconSize} />;
  }
};
