import React, { useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
// import { createDrawerNavigator } from '@react-navigation/drawer';
import {
  AuthParamList,
  DrawerParamsList,
  InnerStackParamList,
} from '../types/AuthParamList';
import { StyleSheet, View, ImageBackground } from 'react-native';
import {
  SplashScreen,
  OnboradinfScreenFirst,
  OnboardingSpleshSecond,
  OnboardingSpleshThird,
  EntryScreen,
  Signup,
  SignUpPhoneScreen,
  SignUpPasswordScreen,
  LoginScreen,
  HomeScreen
} from '../components/screens';
import DrawerContent from './DrawerContent';
import Animated from 'react-native-reanimated';
import { screenHeight, screenWidth } from '../utils/Helpers';
import { navigationRef } from './RootNavigation'

const Stack = createStackNavigator<AuthParamList>();
// const Drawer = createDrawerNavigator<DrawerParamsList>();
const InnerStack = createStackNavigator<InnerStackParamList>();

// const DrawerScreens = (props) => {
//   return (
//     <Animated.View
//       style={StyleSheet.flatten([styles.stack, props.style])}>
//       <InnerStack.Navigator>
//         <InnerStack.Screen
//           name="HomeScreen"
//           component={HomeScreen}
//           options={{ headerShown: false }}

//         />
//         <InnerStack.Screen
//           name="SettingScreen"
//           component={SettingScreen}
//           options={{ headerShown: false }}
//         />
//         <InnerStack.Screen
//           name="NewContactModel"
//           component={NewContactModel}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />

//         <InnerStack.Screen
//           name="GenderSelectionModel"
//           component={GenderSelectionModel}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />

//         <InnerStack.Screen
//           name="VideoToturialModal"
//           component={VideoToturialModal}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateX: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />
//         <InnerStack.Screen
//           name="NewContactModelStep2"
//           component={NewContactModelStep2}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />

//         <InnerStack.Screen
//           name="NewContactModelStep3"
//           component={NewContactModelStep3}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />
//         <InnerStack.Screen
//           name="EditAvtarScreen"
//           component={EditAvtarScreen}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />

//         <InnerStack.Screen
//           name="ProfileModel"
//           component={ProfileModel}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />
//         <InnerStack.Screen
//           name="EditProfileModel"
//           component={EditProfileModel}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />
//         <InnerStack.Screen
//           name="NewCategoriesModel"
//           component={NewCategoriesModel}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />
//         <InnerStack.Screen
//           name="AddPeopleInCatScreen"
//           component={AddPeopleInCatScreen}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />
//         <InnerStack.Screen
//           name="EditCategorieScreen"
//           component={EditCategorieScreen}
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//               // paddingVertical: 20
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateY: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.height, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />
//         <InnerStack.Screen
//           name="SubscriptionScreen"
//           component={SubscriptionScreen}
//           t
//           options={{
//             headerShown: false,
//             cardOverlayEnabled: true,
//             cardStyle: {
//               backgroundColor: 'rgba(0,0,0,0.2)',
//             },
//             cardStyleInterpolator: ({ current, next, layouts }) => {
//               return {
//                 cardStyle: {
//                   transform: [
//                     {
//                       translateX: current.progress.interpolate({
//                         inputRange: [0, 1],
//                         outputRange: [layouts.screen.width, 0],
//                       }),
//                     },
//                   ],
//                 },
//               };
//             },
//           }}
//         />
//         <InnerStack.Screen
//           name={'CategoriesSettingScreen'}
//           component={CategoriesSettingScreen}
//           options={{ headerShown: false }}
//         />
//         <InnerStack.Screen
//           name={'SearchScreen'}
//           component={SearchScreen}
//           options={{ headerShown: false }}
//         />
//         <InnerStack.Screen
//           name={'FavoritesScree'}
//           component={FavoritesScreen}
//           options={{ headerShown: false }}
//         />

//       </InnerStack.Navigator>
//     </Animated.View>
//   );
// };

// const HomeDrawer = () => {
//   const [progress, setProgress] = useState(new Animated.Value(0));
//   const scale = Animated.interpolate(progress, {
//     inputRange: [0, 1],
//     outputRange: [1, 0.8],
//   });
//   const borderRadius = Animated.interpolate(progress, {
//     inputRange: [0, 1],
//     outputRange: [0, 16],
//   });

//   const animatedStyle = { borderRadius, transform: [{ scale }] };
//   return (
//     <Drawer.Navigator
//       drawerContent={(props: any) => {
//         setProgress(props.progress);
//         return <DrawerContent {...props} />;
//       }}
//       drawerContentOptions={{
//         activeBackgroundColor: 'trasnparent',
//       }}
//       // sceneContainerStyle={{ backgroundColor: 'transparent' }}
//       // hideStatusBar={true}
//       // overlayColor='transparent'
//       overlayColor={"rgba(255, 255, 255, 0.4)"}
//       drawerType={'slide'}
//       drawerStyle={{
//         flex: 1,
//         // borderTopRightRadius: 30,
//         // borderBottomRightRadius: 30,
//         width: '60%',
//         backgroundColor: 'white',
//       }}>
//       <Drawer.Screen name="InnerStack">
//         {() => {
//           return <DrawerScreens />;
//         }}
//       </Drawer.Screen>
//     </Drawer.Navigator>
//   );
// };

const Routes = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator>
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="OnboradinfScreenFirst"
          component={OnboradinfScreenFirst}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="OnboardingSpleshSecond"
          component={OnboardingSpleshSecond}
          options={{
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current, next, layouts }) => {
              return {
                cardStyle: {
                  transform: [
                    {
                      translateX: current.progress.interpolate({
                        inputRange: [0, 1],
                        outputRange: [screenWidth, 0],
                      }),
                    },
                  ],
                },
              };
            },
          }}
        />
        <Stack.Screen
          name="OnboardingSpleshThird"
          component={OnboardingSpleshThird}
          options={{
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current, next, layouts }) => {
              return {
                cardStyle: {
                  transform: [
                    {
                      translateX: current.progress.interpolate({
                        inputRange: [0, 1],
                        outputRange: [screenWidth, 0],
                      }),
                    },
                  ],
                },
              };
            },
          }}
        />
        <Stack.Screen
          name="EntryScreen"
          component={EntryScreen}
          options={{
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current, next, layouts }) => {
              return {
                cardStyle: {
                  transform: [
                    {
                      translateX: current.progress.interpolate({
                        inputRange: [0, 1],
                        outputRange: [screenWidth, 0],
                      }),
                    },
                  ],
                },
              };
            },
          }}
        />
        <Stack.Screen
          name="Signup"
          component={Signup}
          options={{
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current, next, layouts }) => {
              return {
                cardStyle: {
                  transform: [
                    {
                      translateX: current.progress.interpolate({
                        inputRange: [0, 1],
                        outputRange: [screenWidth, 0],
                      }),
                    },
                  ],
                },
              };
            },
          }}
        />

        <Stack.Screen
          name="SignUpPhoneScreen"
          component={SignUpPhoneScreen}
          options={{
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current, next, layouts }) => {
              return {
                cardStyle: {
                  transform: [
                    {
                      translateX: current.progress.interpolate({
                        inputRange: [0, 1],
                        outputRange: [screenWidth, 0],
                      }),
                    },
                  ],
                },
              };
            },
          }}
        />

        <Stack.Screen
          name="SignUpPasswordScreen"
          component={SignUpPasswordScreen}
          options={{
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current, next, layouts }) => {
              return {
                cardStyle: {
                  transform: [
                    {
                      translateX: current.progress.interpolate({
                        inputRange: [0, 1],
                        outputRange: [screenWidth, 0],
                      }),
                    },
                  ],
                },
              };
            },
          }}
        />
        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current, next, layouts }) => {
              return {
                cardStyle: {
                  transform: [
                    {
                      translateX: current.progress.interpolate({
                        inputRange: [0, 1],
                        outputRange: [screenWidth, 0],
                      }),
                    },
                  ],
                },
              };
            },
          }}
        />
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{
            headerShown: false,
            cardOverlayEnabled: true,
            cardStyleInterpolator: ({ current, next, layouts }) => {
              return {
                cardStyle: {
                  transform: [
                    {
                      translateX: current.progress.interpolate({
                        inputRange: [0, 1],
                        outputRange: [screenWidth, 0],
                      }),
                    },
                  ],
                },
              };
            },
          }}
        />

      </Stack.Navigator>
    </NavigationContainer >
  );
};

const styles = StyleSheet.create({
  stack: {
    flex: 1,
    shadowColor: '#FFF',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 5,
    overflow: 'hidden',
    backgroundColor: 'transparent',
  },
  drawerStyles: { flex: 1, width: '50%', backgroundColor: 'transparent' },
  drawerItem: { alignItems: 'flex-start', marginVertical: 0 },
  drawerLabel: { color: 'white', marginLeft: -16 },
  avatar: {
    borderRadius: 60,
    marginBottom: 16,
    borderColor: 'transparent',
  },
  image: {
    // position: 'absolute',
    flex: 1,

    width: screenWidth,
    resizeMode: 'contain',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
});

export default Routes;
