import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, Image, SafeAreaView } from "react-native";
import { DrawerContentScrollView } from "@react-navigation/drawer";
import Config from "../utils/Config";
import { normalize, } from "../utils/Helpers";
import { useNavigation } from "@react-navigation/native";
import { moderateScale, scale } from "react-native-size-matters";
import { RootState } from "../redux/reducers";
import { useSelector } from "react-redux";
import { getIcons, Icons } from '../assets/Icons'
import DeviceInfo from 'react-native-device-info';
// import { SafeAreaView } from "react-native-safe-area-context";


const DrawerContent = (props: any) => {
  const user = useSelector(
    (state: RootState) => state.persistedReducer.preference.preference
  );
  const navigation = useNavigation();

  return (
    // <SafeAreaView style={{ flex: 1, backgroundColor: Config.colors.WHITE }} >
    <DrawerContentScrollView
      contentContainerStyle={styles.drawerScrollView}
      showsVerticalScrollIndicator={false}
      {...props}
    >
      <SafeAreaView style={{ flex: 1, backgroundColor: Config.colors.WHITE }} >
        <TouchableOpacity
          onPress={() => {
            props.navigation.closeDrawer();
          }}
          hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
          style={styles.closeBtnStyle}
        >
          {getIcons(Icons.CLOSE_ICON, 25)}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => console.log('im--->', user?.image)
          }
          style={styles.headerView}>
          <View style={[styles.imageBackground2,]}>
            {user?.image ? <Image
              source={{ uri: user?.image }}
              style={styles.userImageStyle}
            /> :
              <Image
                source={require('../assets/Images/ic_appLogo1.jpeg')}
                style={[styles.userImageStyle, {
                  height: scale(120),
                  width: scale(88),
                }]}
                resizeMode='contain'
              />}

            <View style={[styles.imageBackground, { backgroundColor: user?.image ? '#2E3F51' : 'white', }]}>
            </View>

          </View>

          <Text style={styles.headingStyle}>Hello,</Text>
          <Text style={[styles.headingStyle, { color: Config.colors.PRIMERY, fontSize: scale(16) }]}>{user.name ? user.name : 'Guest'}</Text>
        </TouchableOpacity>

        <View style={styles.optionStyle}>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("FavoritesScree");
            }}
            style={[styles.borderView, { marginTop: scale(70) }]}
          >
            {getIcons(Icons.HEART_ICON, 25)}
            <Text style={styles.textStyle}>Favorites</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.borderView}
            onPress={() => {
              console.log(user);
              if (user?.type == "guest") {
                alert("Please login  first")
              } else {
                navigation.navigate('SubscriptionScreen')
              }
            }
            }


          >
            {getIcons(Icons.SUBSCRIPE_ICON, 25)}
            <Text style={styles.textStyle}>Subscription</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.borderView}
            onPress={() => { navigation.navigate('VideoToturialModal') }}
          >
            {getIcons(Icons.PLAYBUTTON_ICON, 25)}
            <Text style={styles.textStyle}>Tutorial</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.borderView}
            onPress={() => {
              // props.navigation.closeDrawer();
              navigation.navigate('SettingScreen')
            }}
          >
            {getIcons(Icons.SETTING_ICON, 25)}
            <Text style={styles.textStyle}>Settings</Text>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.appVersionStyle}>App version {DeviceInfo.getVersion()}</Text>
        </View>
      </SafeAreaView>
    </DrawerContentScrollView>
    // </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  drawerStyles: {
    flex: 1,
    width: "50%",
    backgroundColor: "green",
  },
  drawerItem: {
    alignItems: "flex-start",
    paddingVertical: moderateScale(3),
    fontFamily: Config.fonts.BOLD,
  },
  drawerScrollView: {
    flexGrow: 1,
    backgroundColor: "transparent",
  },
  image: {
    flex: 1,
    resizeMode: "contain",
    justifyContent: "center",
    backgroundColor: "transparent",
  },
  drawerCategories: {
    paddingVertical: moderateScale(15),
  },
  drawerTitle: {
    color: "white",
    fontSize: normalize(19),
    fontFamily: Config.fonts.BOLD,
  },

  borderView: {
    paddingVertical: scale(15),
    flexDirection: 'row'
  },
  textStyle: {
    color: Config.colors.GRAY,
    fontFamily: Config.fonts.BOLD,
    fontSize: normalize(12),
    marginLeft: moderateScale(10)
  },
  optionStyle: {
    flex: 1,
    paddingLeft: moderateScale(15),
  },
  headingStyle: {
    fontFamily: Config.fonts.BOLD,
    fontSize: normalize(12),
    color: Config.colors.GRAY,
    lineHeight: scale(22)
    // marginLeft: moderateScale(10),
    // flexWrap: "wrap",
    // flex: 1,
  },
  headerView: {
    // flexDirection: "row",
    // alignItems: "center",
    marginLeft: moderateScale(15),
    marginTop: moderateScale(40),
  },
  userImageStyle: {
    height: scale(126),
    width: scale(98),
    // resizeMode: "contain",
    borderRadius: scale(25),
    // backgroundColor: 'red',
    // marginBottom: moderateScale(0),
    position: "absolute",
    zIndex: 400,
    top: scale(-26),
    // left: scale(10)
  },
  imageBackground: {
    height: scale(100),
    width: scale(100),
    // resizeMode: "cover",
    borderRadius: scale(25),
    backgroundColor: '#2E3F51',
    marginBottom: scale(10),
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageBackground2: {
    height: scale(100),
    width: scale(100),
    // resizeMode: "cover",
    borderRadius: scale(25),
    // backgroundColor: '#2E3F51',
    marginBottom: scale(10),
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  closeBtnStyle: {
    marginLeft: moderateScale(15),
    marginTop: moderateScale(20),
  },
  appVersionStyle: {
    color: Config.colors.GRAY,
    fontFamily: Config.fonts.REGULAR,
    fontSize: normalize(10),
    marginBottom: scale(20),
    marginLeft: scale(15)
  }
});

export default DrawerContent;
