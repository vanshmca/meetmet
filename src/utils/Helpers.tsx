import React from "react";
import {
    Dimensions,
    PixelRatio,
    Platform,
    StyleSheet,
    Linking,
} from "react-native";
import Config from "./Config";
import { Log } from "./Logger";
// import ImagePicker from "react-native-image-crop-picker";
import { showAlert } from "./AlertHelper";
import { capitalize } from "lodash";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

/** Round-Off Single Digit to Double Digit Number */
const getRoundedDigit = (num: number): string => {
    return String("0" + num).slice(-2);
};

/** Normalize size for different screen sizes */
const normalize = (size: number): number => {
    const scale = screenWidth / 320;
    const newSize = size * scale;
    if (Platform.OS === "ios") {
        return Math.round(PixelRatio.roundToNearestPixel(newSize));
    } else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
    }
};

// const dateConvertor = (date: any) => {
//   if (date) {
//     let prevDate = new Date(date);
//     let dateString = prevDate.toDateString();
//     let dateArray = dateString.split(" ");
//     return dateArray[1] + " " + dateArray[2] + ", " + dateArray[3];
//   }
//   return;
// };

// const epochToJsDate = (timestamp) => {
//   if (timestamp) {
//     const formattedDate = parseInt(timestamp);
//     var date = new Date(formattedDate);
//     date.toString();
//     return dateConvertor(date);
//   }
// };

// const epochToTime = (timestamp) => {
//   if (!timestamp) return;
//   const date = new Date(timestamp);

//   let hours = date.getHours();
//   let minutes: any = date.getMinutes();
//   let ampm = hours >= 12 ? "pm" : "am";

//   hours = hours % 12;
//   hours = hours ? hours : 12; // the hour '0' should be '12'
//   minutes = minutes < 10 ? "0" + minutes : minutes;

//   let strTime = hours + ":" + minutes + " " + ampm;
//   return strTime;
// };

// common function for shadow in android and ios
// const elevationShadowStyle = (elevation) => {
//   return {
//     shadowColor: "#000",
//     shadowOffset: { width: 0, height: 2 },
//     shadowOpacity: 0.5,
//     shadowRadius: 2,
//     elevation: elevation,
//   };
// };

// const getImageUrl = (image: string) => {
//   return Config.server.BASE_URL + image;
// };

// const getAge = (datepickerInstance) => {
//   let todayDate = new Date().getFullYear();
//   let valueYear = new Date(datepickerInstance).getFullYear();
//   let difference = todayDate - valueYear;
//   return difference;
// };

// //get description for subscription screeen with no empty values
// const getDescription = (description: string) => {
//   let descriptionArray = description?.split("\r\n");
//   let filteredArray = descriptionArray?.filter((item) => {
//     return item != "";
//   });
//   return filteredArray;
// };

// const getOptionsArray = (type: String) => {
//   switch (type) {
//     case Config.constants.IMAGE_OPTIONS:
//       return [
//         Config.constants.TAKE_PICTURE,
//         Config.constants.CHOOSE_FROM_GALLARY,
//         Config.constants.CANCEL,
//       ];
//     case Config.constants.DELETE_AND_PROFILE_OPTIONS:
//       return [
//         Config.constants.DELETE_IMAGE,
//         Config.constants.SET_AS_PROFILE,
//         Config.constants.CANCEL,
//       ];
//     case Config.constants.DELETE_OPTIONS:
//       return [Config.constants.DELETE_IMAGE, Config.constants.CANCEL];
//     case Config.constants.REPLY_OPTIONS:
//       return [Config.constants.REPLY_MESSAGE, Config.constants.CANCEL];
//     default:
//       return [];
//   }
// };

// const getFullName = (firstName: string, lastName?: string) => {
//   if (!lastName) return capitalize(firstName);
//   else return capitalize(firstName) + " " + capitalize(lastName);
// };

// const getCbName = (userName: string) => {
//   return capitalize(userName);
// };

// const pickSingle = async (cropit, circular = false) => {
//   return new Promise(async (resolve, reject) => {
//     try {
//       let res = await ImagePicker.openPicker({
//         maxFiles: 5,
//         cropping: cropit,
//       });
//       if (res) resolve(res);
//     } catch (error) {
//       if (
//         error.message === "Required permission missing" &&
//         Platform.OS === "android"
//       ) {
//         const res = await showAlert(
//           Config.permissions.permission_image,
//           "Alert",
//           true,
//           "Open Settings"
//         );
//         if (res) {
//           Linking.openSettings().catch(() => Log("cannot"));
//         }
//       }
//     }
//   });
// };

// /**
//  * Function to pick single image from camera
//  */
// const pickFromCamera = async (multiple?: boolean, circular = true) => {
//   return new Promise(async (resolve, reject) => {
//     try {
//       let res = await ImagePicker.openCamera({
//         width: 500,
//         height: 500,
//         cropping: true,
//       });
//       if (res) resolve(res);
//     } catch (error) {
//       if (
//         error.message === "Required permission missing" &&
//         Platform.OS === "android"
//       ) {
//         const res = await showAlert(
//           Config.permissions.permission_camera,
//           "Alert",
//           true,
//           "Open Settings"
//         );
//         if (res) {
//           Linking.openSettings().catch(() => Log("cannot"));
//         }
//       }
//     }
//   });
// };

export {
    screenHeight,
    screenWidth,
    getRoundedDigit,
    normalize,
    //   getOptionsArray,
    //   getAge,
    //   epochToJsDate,
    //   elevationShadowStyle,
    //   getImageUrl,
    //   pickFromCamera,
    //   pickSingle,
    //   getDescription,
    //   epochToTime,
    //   getFullName,
    //   getCbName,
};
