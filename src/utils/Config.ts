import { LOCAL_URL } from '../security'
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';
export default {
  server: {
    BASE_URL: LOCAL_URL,
  },
  // fonts: {
  //   BOLD: '',
  //   SEMI_BOLD: '',
  //   MEDIUM: '',
  //   REGULAR: '',
  // },
  fonts: {
    BOLD: 'Poppins-Bold',
    SEMI_BOLD: 'Poppins-SemiBold',
    MEDIUM: 'Poppins-Medium',
    REGULAR: 'Poppins-Regular',
    LIGHT: "Poppins-Light",
  },
  colors: {
    AppBlueColor: '#3D6EA9',
    textColorGray: '#606060',
    lightGray: '#E6E6E6',
    textColorTitle: '#202020',
    whiteColor: '#FFFFFF',
    backgroundColor: '#F8F8F8'

  },
  size: {
    moderateScale,
    scale,
    verticalScale
  },
  text: {
    HomeScreenText: 'Start your way from adding the\nfirst contact to your contacts list',
    Are_You_Sure: 'Are You Sure !',
    Change_Name: 'Change Name',
    Success: 'Success',
    Name: 'Name',
    Email: 'Email',
    Login_via: "You've logged in via",
    LogOut: "You've logged out",
    PrivacyPolicy: 'Privacy Policy',
    TermOfUse: 'Terms of Use',
    Feedback: 'Feedback',
    Help: 'Help',
    Logout: 'Logout',
    Signin: 'Signin',
    deleteAlerttext: 'Are you sure you want to delete it?',
    create_new_categories: 'Create New Category',
    AddRemovePeople: 'Add/Remove Pepole',
    Add_Favorites: 'Add Favorites',
    empty_search_text: "Didn't find? Add now",
  },
  constants: {
    facebook: 'facebook',
    apple: 'apple',
    google: 'google',
  },
  regex: {
    regExEmail: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    nameRegEx: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
    numericRegEx: /\d/,
    specialCharRegEx: /[^a-zA-Z ]/,
  },
  error: {
    error_internet_connection:
      'Something went wrong!! Please check your internet and try again',
    error_empty_first_name: 'Please enter Name',
    error_first_name_numeric: 'Name cannot have numeric values',
    error_first_name_specialChar: 'Name cannot have special characters',
    error_first_name_long:
      'Name field cannot have more than 100 characters',
    error_empty_know_from: 'Please enter Know From',
    error_know_from_long:
      'Know from field cannot have more than 200 characters',
    error_bgcolor: 'Please select any color',
    error_empty_characteristic: 'Please enter Characteristic',
    error_characteristic_long:
      'Characteristic field cannot have more than 500 characters',
    error_empty_info: 'Please enter Info',
    error_info_long:
      'Info field cannot have more than 500 characters',
    error_empty_notes: 'Please enter Notes',
    error_notes_long:
      'Notes field cannot have more than 500 characters',
    error_empty_email: 'Please enter the email',
    error_invalid_email: 'Please enter a valid email address',
    error_empty_Cat_title: 'Please enter Title',
    error_empty_Cat_bgcolor: 'Please select any color',
  }
};
