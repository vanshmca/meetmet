import config from './Config';
import { contact, User, Editcontact } from '../types/modals/User'
import { getAge } from './Helpers';


export interface ContactType {
  name: string
  info: string
  characterstics: string
  gender: string
  notes: string
  image: string
  bgcolor: string
  knowfrom: string
}
export interface ValidationContactType {
  nameError: string
  infoError: string
  charactersticsError: string
  genderError: string
  notesError: string
  imageError: string
  bgcolorError: string
  knowFromError: string
}

export interface EditProfileType {
  name: string
  info: string
  characterstics: string
  gender: string
  notes: string
  image: string
  email: string
  knowfrom: string
}
export interface ValidationEditProfileType {
  nameError: string
  infoError: string
  charactersticsError: string
  genderError: string
  notesError: string
  imageError: string
  emailError: string
  knowFromError: string
}

export interface CategoriesType {
  title: string;
  bgColor: string;
  id?: string
}
export interface ValidateCategoriesType {
  titleError: string;
  bgColorError: string;
}



export const validateAddContacts = (detailObj: contact | Editcontact): Promise<ValidationContactType | string> => {
  const obj: ValidationContactType = {};

  return new Promise((resolve, reject) => {
    // if (!detailObj.name) {
    //   obj.nameError = config.error.error_empty_first_name;
    // }
    // else if (detailObj.name.trim() == '') {
    //   obj.nameError = config.error.error_empty_first_name;
    // } else if (
    //   !obj.nameError &&
    //   config.regex.numericRegEx.test(detailObj.name)
    // ) {
    //   obj.nameError = config.error.error_first_name_numeric;
    // } else if (
    //   !obj.nameError &&
    //   config.regex.specialCharRegEx.test(detailObj.name)
    // ) {
    //   obj.nameError = config.error.error_first_name_specialChar;
    // } else if (!obj.nameError && detailObj.name.length >= 100) {
    //   obj.nameError = config.error.error_first_name_long;
    // }
    // if (!detailObj.knowfrom) {
    //   obj.knowFromError = config.error.error_empty_know_from;
    // } else if (detailObj.knowfrom.trim() == "") {
    //   obj.knowFromError = config.error.error_empty_know_from;
    // }
    
    // if (!detailObj.characterstics) {
    //   obj.charactersticsError = config.error.error_empty_characteristic;
    // } else if (detailObj.characterstics.trim() == "") {
    //   obj.charactersticsError = config.error.error_empty_characteristic;
    // }
    // else if (!obj.charactersticsError && detailObj.characterstics.length >= 500) {
    //   obj.charactersticsError = config.error.error_characteristic_long;
    // }
    // if (!detailObj.info) {
    //   obj.infoError = config.error.error_empty_info;
    // } else if (detailObj.info.trim() == "") {
    //   obj.infoError = config.error.error_empty_info;
    // }
    // else if (!obj.infoError && detailObj.info.length >= 500) {
    //   obj.infoError = config.error.error_info_long;
    // }
    // if (!detailObj.notes) {
    //   obj.notesError = config.error.error_empty_notes;
    // } else if (detailObj.notes.trim() == "") {
    //   obj.notesError = config.error.error_empty_notes;
    // }
    // else if (!obj.notesError && detailObj.notes.length >= 500) {
    //   obj.notesError = config.error.error_notes_long;
    // }

    if (Object.keys(obj).length != 0) {
      resolve({
        code: 400,
        validationObject: obj
      });
    } else {
      resolve({ code: 200 });
    }
  });
};

export const validateEditProgile = (detailObj: User): Promise<ValidationEditProfileType | string> => {
  const obj: ValidationEditProfileType = {};

  return new Promise((resolve, reject) => {
    if (!detailObj.name) {
      obj.nameError = config.error.error_empty_first_name;
    } else if (detailObj.name.trim() == "") {
      obj.nameError = config.error.error_empty_first_name;
    }
    else if (
      !obj.nameError &&
      config.regex.numericRegEx.test(detailObj.name)
    ) {
      obj.nameError = config.error.error_first_name_numeric;
    } else if (
      !obj.nameError &&
      config.regex.specialCharRegEx.test(detailObj.name)
    ) {
      obj.nameError = config.error.error_first_name_specialChar;
    } else if (!obj.nameError && detailObj.name.length >= 100) {
      obj.nameError = config.error.error_first_name_long;
    }
    if (!detailObj.characterstics) {
      obj.charactersticsError = config.error.error_empty_characteristic;
    } else if (detailObj.characterstics.trim() == "") {
      obj.charactersticsError = config.error.error_empty_characteristic;
    }
    else if (!obj.charactersticsError && detailObj.characterstics.length >= 500) {
      obj.charactersticsError = config.error.error_characteristic_long;
    }
    if (!detailObj.info) {
      obj.infoError = config.error.error_empty_info;
    } else if (detailObj.info.trim() == "") {
      obj.infoError = config.error.error_empty_info;
    }
    else if (!obj.infoError && detailObj.info.length >= 500) {
      obj.infoError = config.error.error_info_long;
    }
    if (!detailObj.notes) {
      obj.notesError = config.error.error_empty_notes;
    } else if (detailObj.notes.trim() == "") {
      obj.notesError = config.error.error_empty_notes;
    }
    else if (!obj.notesError && detailObj.notes.length >= 500) {
      obj.notesError = config.error.error_notes_long;
    }
    if (!detailObj.email) {
      obj.emailError = config.error.error_empty_email;
    } else if (detailObj.email.trim() == "") {
      obj.emailError = config.error.error_empty_email;
    }
    else if (!obj.emailError && !config.regex.regExEmail.test(detailObj.email)) {
      obj.emailError = config.error.error_invalid_email;
    }

    if (Object.keys(obj).length != 0) {
      resolve({
        code: 400,
        validationObject: obj
      });
    } else {
      resolve({ code: 200 });
    }
  });
};

export const validateCategorie = (data: CategoriesType): Promise<ValidateCategoriesType | string> => {
  const obj: ValidateCategoriesType = {};

  return new Promise((resolve, reject) => {
    if (!data.title) {
      obj.titleError = config.error.error_empty_Cat_title;
    } else if (data.title.trim() == '')
      obj.titleError = config.error.error_empty_Cat_title;
    if (!data.bgColor) {
      obj.bgColorError = config.error.error_empty_Cat_bgcolor;
    }

    if (Object.keys(obj).length != 0) {
      resolve({
        code: 400,
        validationObject: obj
      });
    } else {
      resolve({ code: 200 });
    }
  });
};


