import React from 'react';
import Routes from '../navigation';
import { Provider } from 'react-redux';
import store from '../redux/reducers';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
// import { ActionSheetProvider } from '@expo/react-native-action-sheet';
import { LogBox, StatusBar } from 'react-native';

// Initialize the module (needs to be done only once)
const App = () => {
  // splashScreen.hide();
  LogBox.ignoreAllLogs(true);
  const persistor = persistStore(store);
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <StatusBar backgroundColor="#F8F8F8" barStyle="dark-content" />
        <Routes />
      </PersistGate>
    </Provider>
  );
};

export default App;
