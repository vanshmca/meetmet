import React from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'
import Config from '../../utils/Config'
import { normalize } from '../../utils/Helpers'
import LinearGradient from 'react-native-linear-gradient';

interface Props {
    onPress: () => void
    text: string
    style?: object
}

const Button = ({ onPress, text, style }: Props) => {
    return (
        <LinearGradient
            start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }}
            colors={["#C0D2E9", "#104586"]}
            style={[styles.Linbutton, style]}
        >
            <TouchableOpacity
                onPress={onPress}
                style={styles.button}>
                <Text style={styles.text}>{text}</Text>
            </TouchableOpacity>
        </LinearGradient>

    )
}


const styles = StyleSheet.create({
    button: {
        height: Config.size.verticalScale(45),
        width: '100%',
        alignItems: 'center',
        // backgroundColor: Config.colors.AppBlueColor,
        justifyContent: 'center',
        // borderRadius: Config.size.scale(5)

    },
    text: {
        color: 'white',
        fontSize: normalize(15),
        fontFamily: Config.fonts.MEDIUM,
    },
    Linbutton: {
        height: Config.size.verticalScale(45),
        width: '100%',
        alignItems: 'center',
        // backgroundColor: Config.colors.AppBlueColor,
        justifyContent: 'center',
        borderRadius: Config.size.scale(5),
        elevation: Config.size.scale(4),
        shadowOffset: { width: 0, height: 4 },
        shadowColor: Config.colors.lightGray,
        shadowOpacity: Config.size.scale(4),
    }
})

export default Button