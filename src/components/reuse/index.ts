import OnboardHeader from './OnboardHeader'
import Button from './Button'
import TextInputComp from './TextInputComp'
import SocialLoginButton from './SocialLoginButton'
import ErrorPopup from './ErrorPopup'

export {
    OnboardHeader,
    Button,
    TextInputComp,
    SocialLoginButton,
    ErrorPopup,
}