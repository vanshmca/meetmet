import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import Config from '../../utils/Config'
import { normalize } from '../../utils/Helpers'
import { getIcons, Icons } from '../../assets/Icons'


interface Props {
    errorText: string;
    onCencalPress: () => void;
}

const ErrorPopup = ({ errorText, onCencalPress }) => {
    return (
        <View style={styles.mainView}>
            <Text style={styles.text}>{errorText}</Text>
            <TouchableOpacity
                onPress={onCencalPress}
                hitSlop={{ top: 20, bottom: 20, left: 50, right: 10 }}
                style={styles.button}>
                {getIcons(Icons.CLOSE_ICON, 10)}
            </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    text: {
        color: '#E03C4C',
        fontFamily: Config.fonts.MEDIUM,
        marginLeft: Config.size.scale(10),
        fontSize: normalize(10)
    },
    mainView: {
        backgroundColor: '#F9EAEC',
        height: Config.size.verticalScale(40),
        // alignItems: 'center',
        justifyContent: 'center',
        borderRadius: Config.size.scale(5),
        borderWidth: 1,
        borderColor: '#E03C4C'
    },
    button: {
        position: 'absolute',
        right: Config.size.scale(10)
    }
})

export default ErrorPopup