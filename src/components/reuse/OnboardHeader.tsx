import React from 'react'
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native'
import { OnboradinfScreenFirst } from '../screens'
import Config from '../../utils/Config'
import { getIcons, Icons } from '../../assets/Icons'


interface Props {
    isBack?: boolean
    onBackPress?: () => void;
    isSkip?: boolean
    isBlueBackIcon?: boolean
    skipPress?: () => void
}

const OnboardHeader = ({ isBack = true, onBackPress, isSkip = true, isBlueBackIcon = false, skipPress }: Props) => {
    return (
        <View style={styles.mainContainer}>
            {isBack && <TouchableOpacity
                onPress={onBackPress}
                hitSlop={{ top: 20, bottom: 20, right: 15, left: 30 }}
                style={styles.BackButton}>
                {isBlueBackIcon ? getIcons(Icons.BACK_BLUE_BUTTON, 18) : getIcons(Icons.BACK_BUTTON, 18)}
            </TouchableOpacity>}
            {isSkip && <TouchableOpacity
                onPress={skipPress}
                hitSlop={{ top: 20, bottom: 20, right: 15, left: 30 }}
                style={styles.skipButton}>
                <Text style={styles.text}>Skip</Text>
            </TouchableOpacity>}
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        height: Config.size.verticalScale(50),
        justifyContent: 'center'
    },
    text: {
        color: Config.colors.AppBlueColor,
        fontFamily: Config.fonts.REGULAR
    },
    skipButton: {
        position: 'absolute',
        right: Config.size.scale(15)
    },
    BackButton: {
        position: 'absolute',
        left: Config.size.scale(15)
    }
})


export default OnboardHeader