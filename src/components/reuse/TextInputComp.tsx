import React from 'react';
import {
  ViewStyle,
  StyleProp,
  TextInput,
  KeyboardTypeOptions,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Config from '../../utils/Config';
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';
import { getIcons, Icons } from '../../assets/Icons';
import { normalize } from '../../utils/Helpers'


interface Props {
  value: string | undefined;
  editable?: boolean;
  placeholder: string;
  style?: StyleProp<ViewStyle>;
  onChangeText?: (text: string) => void;
  keyboardType?: KeyboardTypeOptions;
  secureTextEntry?: boolean;
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters';
  maxLength?: number;
  getInputRef?: (ref: TextInput | null) => void;
  onSubmitEditing?: () => void;
  returnKeyType?: 'none' | 'done' | 'next';
  validationMessage?: string;
  multiline?: boolean;
  text?: string;
  is_icon?: any
  userData?: any
  onPress?: (text: string) => void;
  isSearchIcon?: boolean
  textStyle?: StyleProp<ViewStyle>;
  containerStyle?: StyleProp<ViewStyle>;
}

const TextInputComp = ({
  value,
  placeholder,
  onChangeText,
  keyboardType,
  secureTextEntry = false,
  autoCapitalize,
  maxLength,
  style,
  editable = true,
  getInputRef,
  onSubmitEditing,
  returnKeyType,
  validationMessage,
  multiline,
  text,
  is_icon,
  userData,
  onPress,
  isSearchIcon,
  textStyle,
  containerStyle
}: Props) => {
  return (
    <View
      style={[styles.mainContainer, containerStyle]}>



      <View style={{ height: verticalScale(40), borderRadius: Config.size.scale(5), backgroundColor: Config.colors.whiteColor, borderWidth: 1, borderColor: Config.colors.lightGray, paddingLeft: Config.size.scale(10), justifyContent: 'center' }}>
        <Text style={styles.titleText}>{text}</Text>
        <TextInput
          value={value}
          maxLength={maxLength}
          onChangeText={onChangeText}
          keyboardType={keyboardType}
          autoCapitalize={autoCapitalize}
          secureTextEntry={secureTextEntry}
          onSubmitEditing={onSubmitEditing}
          multiline={multiline}
          editable={editable}
          placeholderTextColor={Config.colors.textColorTitle}
          style={[
            {
              // color: Config.colors.GRAY,
              fontSize: scale(10),
              // backgroundColor: 'red',
              padding: Config.size.verticalScale(0),
              // height: Config.size.verticalScale(30),
              width: "90%",
              fontFamily: Config.fonts.REGULAR
              // marginLeft: scale(10)
            },
            style,
          ]}
          returnKeyType={returnKeyType}
          placeholder={placeholder}
          textContentType="telephoneNumber"
          ref={(ref) => {
            if (getInputRef) getInputRef(ref);
          }}
        // containerStyle={{
        //   backgroundColor: 'red',
        // }}
        />
        {is_icon ? <TouchableOpacity
          onPress={onPress}
          hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
          style={[styles.iconStyle]}>
          {is_icon}
          {/* <Text>icon</Text> */}
        </TouchableOpacity> : null}
      </View>


      {/* <View style={[
        {
          height: 50,
          // color: Config.colors.GRAY,
          borderWidth: 1,
          borderBottomColor: validationMessage ? 'red' : null,
          // fontSize: 16,
          borderColor: editable ? Config.colors.GRAY : 'black',
          borderRadius: 10,
          paddingLeft: moderateScale(10),
          flexDirection: 'row',
          alignItems: 'center'
          // ...(!editable && { backgroundColor: 'white' }),
        },
        style,
      ]}>
        {isSearchIcon ? <TouchableOpacity
          // onPress={userData ? onPressEdit : null}
          // hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
          style={{ marginRight: scale(5) }}
        >
          {getIcons(Icons.SEARCH_ICON)}
        </TouchableOpacity> : null}
        <TextInput
          value={value}
          maxLength={maxLength}
          onChangeText={onChangeText}
          keyboardType={keyboardType}
          autoCapitalize={autoCapitalize}
          secureTextEntry={secureTextEntry}
          onSubmitEditing={onSubmitEditing}
          multiline={multiline}
          editable={editable}
          placeholderTextColor={Config.colors.GRAY}
          style={[
            {
              color: Config.colors.GRAY,
              fontSize: scale(11.5),
              // marginLeft: scale(10)
            },
            style,
          ]}
          returnKeyType={returnKeyType}
          placeholder={placeholder}
          textContentType="telephoneNumber"
          ref={(ref) => {
            if (getInputRef) getInputRef(ref);
          }}
        />
        {is_icon ? <TouchableOpacity
          onPress={userData ? onPressEdit : null}
          // hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
          style={[styles.iconStyle, { backgroundColor: userData ? Config.colors.PrimaryButton : Config.colors.GRAY }]}>
          {getIcons(Icons.EDIT_ICON)}
        </TouchableOpacity> : null}
      </View> */}
      {validationMessage && (
        <Text
          style={styles.text}>
          {validationMessage}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    // flex: 1,
    width: '100%',
    // paddingStart: 25,
    marginTop: moderateScale(10),


  },
  text: {
    marginTop: moderateScale(4),
    marginLeft: moderateScale(5),
    color: 'red',
    fontSize: moderateScale(11),
    // fontFamily: Config.fonts.REGULAR,
  },
  textHeading: {
    color: Config.colors.PRIMERY,
    fontFamily: Config.fonts.BOLD,
    fontSize: scale(13),
    marginLeft: moderateScale(15),
    marginBottom: scale(5)

  },
  iconStyle: {
    // backgroundColor: Config.colors.PrimaryButton,
    // height: moderateScale(30),
    // width: moderateScale(30),
    position: "absolute",
    right: moderateScale(10),
    // borderRadius: moderateScale(5),
    justifyContent: 'center',
    alignItems: 'center',
    bottom: verticalScale(5)
  },
  searchIconStyle: {
    backgroundColor: Config.colors.PrimaryButton,
    height: moderateScale(30),
    width: moderateScale(30),
    position: "absolute",
    left: moderateScale(10),
    borderRadius: moderateScale(5),
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText: {
    fontFamily: Config.fonts.LIGHT,
    color: Config.colors.textColorGray,
    fontSize: normalize(8),
    marginTop: Config.size.verticalScale(4)
  }

})

export default TextInputComp
