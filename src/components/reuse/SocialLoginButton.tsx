import React from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'
import Config from '../../utils/Config'
import { normalize } from '../../utils/Helpers'
import { getIcons, Icons } from '../../assets/Icons';

interface Props {
    onPress: () => void
    text: string
    style?: object
    icon: any
}

const SocialLoginButton = ({ onPress, text, style, icon }: Props) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={[styles.button, style]}>
            <View style={{ position: 'absolute', left: Config.size.scale(20) }}>
                {icon}
            </View>
            <Text style={styles.text}>{text}</Text>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    button: {
        height: Config.size.verticalScale(45),
        width: '100%',
        alignItems: 'center',
        backgroundColor: Config.colors.whiteColor,
        justifyContent: 'center',
        elevation: Config.size.scale(4),
        borderRadius: Config.size.scale(5),
        flexDirection: 'row',
        shadowOffset: { width: 0, height: 4 },
        shadowColor: Config.colors.lightGray,
        shadowOpacity: Config.size.scale(4),
    },
    text: {
        color: Config.colors.textColorGray,
        fontSize: normalize(14),
        fontFamily: Config.fonts.MEDIUM,
    },
    Linbutton: {
        height: Config.size.verticalScale(45),
        width: '100%',
        alignItems: 'center',
        // backgroundColor: Config.colors.AppBlueColor,
        justifyContent: 'center',
        borderRadius: Config.size.scale(5)
    }
})

export default SocialLoginButton