import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView } from 'react-native'
import HOC from '../../HOC'
import { OnboardHeader, Button } from '../../reuse'
import Config from '../../../utils/Config'
import { normalize } from '../../../utils/Helpers'
import { useNavigation } from '@react-navigation/native';


/**
 * Onboarding splesh 2 screen 
 */

const OnboardingSpleshSecond = () => {
    const navigation = useNavigation();

    const onNextPress = () => {
        navigation.navigate('OnboardingSpleshThird')
    }

    const onBackPress = () => {
        navigation.goBack()
    }
    const onSkipPress = () => {
        navigation.reset({
            index: 0,
            routes: [{ name: "EntryScreen" }],
        });
    }
    return (
        <View style={styles.mainContainer}>
            <OnboardHeader onBackPress={onBackPress} skipPress={onSkipPress} />
            <View style={styles.innerView}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={styles.text}>
                        Efficiently Share medical records efficiently
                    </Text>
                    <Text style={styles.bottomText}>Securely share your medical record with your provider</Text>
                    <Image
                        source={require('../../../assets/Images/onboardSecond.png')}
                        style={styles.image}
                        resizeMode="contain"
                    />
                    <Button
                        text={"Next"}
                        onPress={onNextPress}
                        style={{ marginTop: Config.size.verticalScale(120) }}
                    />
                    <View style={styles.bottomDotsView}>
                        <View style={styles.dots} />
                        <View style={[styles.dots, { backgroundColor: Config.colors.AppBlueColor }]} />
                        <View style={styles.dots} />
                    </View>
                </ScrollView>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    innerView: {
        paddingHorizontal: Config.size.scale(15)
    },
    text: {
        fontSize: normalize(18),
        fontFamily: Config.fonts.MEDIUM,
        color: Config.colors.textColorGray
    },
    bottomText: {
        marginTop: Config.size.verticalScale(10),
        fontFamily: Config.fonts.REGULAR,
        color: Config.colors.textColorGray
    },
    image: {
        height: Config.size.verticalScale(200),
        width: Config.size.scale(250),
        alignSelf: 'center',
        marginTop: Config.size.verticalScale(80)
    },
    bottomDotsView: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        marginTop: Config.size.verticalScale(10)
    },
    dots: {
        height: Config.size.scale(8),
        width: Config.size.scale(8),
        borderWidth: 1,
        borderColor: Config.colors.AppBlueColor,
        borderRadius: Config.size.scale(4),
        margin: Config.size.scale(2),
    }
})


export default HOC(OnboardingSpleshSecond)