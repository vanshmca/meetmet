import SplashScreen from './Auth/SplashScreen';
import OnboradinfScreenFirst from './OnboardingScreens/OnboardingSplesh1'
import OnboardingSpleshSecond from './OnboardingScreens/OnboardingSplesh2'
import OnboardingSpleshThird from './OnboardingScreens/OnboardingSplesh3'
import EntryScreen from './OnboardingScreens/EntryScreen'
import Signup from './Auth/Signup'
import SignUpPhoneScreen from './Auth/SignUpPhoneScreen';
import SignUpPasswordScreen from './Auth/SignUpPasswordScreen';
import LoginScreen from './Auth/LoginScreen';
import HomeScreen from './HomeScreen';

export {
  SplashScreen,
  OnboradinfScreenFirst,
  OnboardingSpleshSecond,
  OnboardingSpleshThird,
  EntryScreen,
  Signup,
  SignUpPhoneScreen,
  SignUpPasswordScreen,
  LoginScreen,
  HomeScreen,
};
