/**
 * Component to display splash screen and handle push notification actions and other
 * actions required at the start of the application
 */

import React, { useEffect } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import splashScreen from "react-native-splash-screen";
import { StackNavigationProp } from "@react-navigation/stack";
import { AuthParamList } from "../../../types/AuthParamList";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import { RootState } from "../../../redux/reducers";
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';


let appOpenedFromNotification = false;

const SplashScreen = () => {

  type SplashScreen = StackNavigationProp<AuthParamList, "SplashScreen">;
  const navigation = useNavigation<SplashScreen>();
  const userToken = useSelector((state: RootState) => state.persistedReducer.token);
  const resetStackForUser = () => {
    let routeName = userToken ? "HomeDrawer" : "OnboradinfScreenFirst";
    navigation.reset({
      index: 0,
      routes: [{ name: routeName }],
    });

  }

  useEffect(() => {

    //for hide spleshscreen and then go to socialLogin screen
    setTimeout(() => {
      splashScreen.hide();
      resetStackForUser()

    }, 2000);
  }, []);

  return (
    <View style={style.mainView}>
      <Image
        source={require("../../../assets/Images/logo.png")}
        style={style.image}
      />
    </View>
  )
};


const style = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    height: verticalScale(50),
    width: '50%'
  }
})

export default SplashScreen;
