import React, { useEffect } from 'react'
import { AccessToken, LoginManager } from 'react-native-fbsdk';

import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-community/google-signin';
import Config from '../../../utils/Config'



//for facebook login
async function facebookLogin() {
    try {
        // dispatch(setLoading(true))
        LoginManager.logOut();
        let res
        let info
        const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
        if (result.isCancelled)
            throw new Error('User cancelled request');

        const data = await AccessToken.getCurrentAccessToken();
        console.log(data, 'FBdata')
        if (!data)
            throw new Error('Something went wrong obtaining the users access token');
        const provider = Config.constants.facebook
        fetch('https://graph.facebook.com/v2.5/me?fields=email,name,first_name,middle_name,last_name,gender,address,picture.type(large)&access_token=' + data.accessToken)
            .then((response) => {
                response.json().then(async (json) => {
                    // dispatch(storeUserDetails({ ...json, provider: provider }))
                    info = json
                    console.log(info, "dataFB")
                    return { data, info }
                    // res = await dispatch(socialLoginAction(data.userID, provider, info.name, info.email))

                })
            })
            .catch(() => {
                console.log('ERROR GETTING DATA FROM FACEBOOK')
            })


        console.log(data, 'data')
    } catch (e) {
        if (AccessToken.getCurrentAccessToken() != null) {
            if (e.toString().includes("Error:")) {
                let error = e.toString().replace("Error:", "")
                console.log(error)
            }
        }
    }
}

//for google login
const googleSignIn = async () => {
    GoogleSignin.configure();
    try {
        await GoogleSignin.signOut()
        await GoogleSignin.hasPlayServices();
        // const { accessToken, idToken } = await GoogleSignin.getTokens();
        const userInfo = await GoogleSignin.signIn();
        console.log(userInfo, "googleDataInfo")
        const provider = Config.constants.google
        console.log(userInfo, "infoUser")
        return userInfo
        // let res = await dispatch(socialLoginAction(userInfo.user.id, provider, userInfo.user.name, userInfo.user.email))

    } catch (error) {
        console.log(error, "error"
        )
    }

}

//on sign in with apple
const appleSignIn = async (result) => {
    console.log("apple==>", result);

    let provider = Config.constants.apple

    //check if any error occurs during signin
    if (Object.keys(result).includes('user')) {
        // let res = await dispatch(socialLoginAction(result.user, provider, result.firstName ? result.firstName: 'MeetmetUser', result.email ? result.email : result.user+'@buca.com'))


    }
};


export {
    appleSignIn,
    googleSignIn,
    facebookLogin
}




