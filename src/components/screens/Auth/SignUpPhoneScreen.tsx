import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import HOC from '../../HOC'
import Config from '../../../utils/Config'
import { normalize } from '../../../utils/Helpers'
import { OnboardHeader, Button, TextInputComp, SocialLoginButton, ErrorPopup } from '../../reuse'
import { getIcons, Icons } from '../../../assets/Icons'
import { useNavigation, useRoute } from '@react-navigation/native';


const SignUpPhoneScreen = () => {
    const navigation = useNavigation();
    const routes = useRoute()
    const [phoneNumber, setPhoneNumber] = useState('')
    const [error, setError] = useState("")


    const onBackPress = () => {
        navigation.goBack();
    }

    const onNextPress = () => {
        if (phoneNumber.trim() == '') {
            setError("Please enter phone number")
        } else if (phoneNumber.length < 10 || phoneNumber.length > 13) {
            setError("Please enter valid phone number")
        } else
            navigation.navigate('SignUpPasswordScreen', { data: { email: routes?.params?.email, phone: phoneNumber } })
        console.log(routes?.params?.email);

    }

    const onLoginPress = () => {
        navigation.navigate('LoginScreen')
    }

    const onCancelPress = () => {
        setError("")
    }

    return (
        <View style={{ flex: 1 }}>
            <OnboardHeader isSkip={false} isBlueBackIcon={true} onBackPress={onBackPress} />
            <View style={styles.mainView}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <Text style={styles.title}>Continue</Text>
                    <Text style={styles.subTitle}>Enter your phone number to have a </Text>
                    <Text style={[styles.subTitle, { marginBottom: Config.size.scale(10) }]}>secure and personalized experience.</Text>
                    <View style={{ marginVertical: Config.size.verticalScale(10) }}>
                        {error != "" && <ErrorPopup
                            errorText={error}
                            onCencalPress={onCancelPress}
                        />}
                        <TextInputComp
                            text={'Phone number'}
                            placeholder={"(XXXX) XXX-XXXX"}
                            value={phoneNumber}
                            is_icon={getIcons(Icons.PHONE_ICON, 18)}
                            onChangeText={(text) => setPhoneNumber(text)}
                        />
                    </View>
                    <Button
                        text={"Next"}
                        onPress={onNextPress}
                        style={{ marginTop: Config.size.verticalScale(10) }}
                    />
                    <View style={styles.signinView}>
                        <Text style={styles.textHeding}>
                            Already have an account?
                        </Text>
                        <TouchableOpacity
                            onPress={onLoginPress}
                            hitSlop={{ top: 12, bottom: 20, left: 50, right: 50 }}>
                            <Text style={styles.textSignin}>
                                Login
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.middleView}>
                        <View style={styles.horizontalLine}></View>
                        <Text style={styles.middleText}>OR</Text>
                        <View style={styles.horizontalLine}></View>
                    </View>

                    <SocialLoginButton
                        text={"Sign up with Google"}
                        //onPress={onSignupPress}
                        style={{ marginVertical: Config.size.verticalScale(8) }}
                        icon={getIcons(Icons.GOOGLE_ICON, 20)}
                    />
                    <SocialLoginButton
                        text={"Sign up with Apple"}
                        //onPress={onSignupPress}
                        style={{ marginVertical: Config.size.verticalScale(8) }}
                        icon={getIcons(Icons.APPLE_ICON, 20)}
                    />
                    <SocialLoginButton
                        text={"Sign up with Facebook"}
                        //onPress={onSignupPress}
                        style={{ marginVertical: Config.size.verticalScale(8) }}
                        icon={getIcons(Icons.FACEBOOK_ICON, 20)}
                    />
                </ScrollView>
            </View>

        </View>

    )
}


const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // backgroundColor: Config.colors.backgroundColor,
        paddingHorizontal: Config.size.scale(15),
    },
    title: {
        fontSize: Config.size.scale(24),
        fontFamily: Config.fonts.MEDIUM
    },
    subTitle: {
        fontSize: Config.size.scale(14),
        color: Config.colors.textColorGray,
    },
    textSignin: {
        color: Config.colors.AppBlueColor,
        marginVertical: Config.size.verticalScale(10),
        // padding: Config.size.scale(2),
        fontFamily: Config.fonts.REGULAR,
        marginLeft: Config.size.scale(2)
    },
    signinView: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
    },
    horizontalLine: {
        flex: 1,
        height: Config.size.scale(1),
        backgroundColor: "#EDEDED",
    },
    middleText: {
        color: Config.colors.textColorGray,
        fontSize: Config.size.scale(16),
        fontWeight: 'bold',
        marginHorizontal: Config.size.scale(10)
    },
    middleView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: Config.size.verticalScale(10)
    },
    textHeding: {
        marginVertical: Config.size.verticalScale(10),
        fontFamily: Config.fonts.REGULAR
    }
})


export default HOC(SignUpPhoneScreen)