import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import HOC from '../../HOC'
import Config from '../../../utils/Config'
import { normalize } from '../../../utils/Helpers'
import { OnboardHeader, Button, TextInputComp, SocialLoginButton, ErrorPopup } from '../../reuse'
import { getIcons, Icons } from '../../../assets/Icons'
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { Login } from '../../../redux/actions/AuthActions'



const LoginScreen = () => {
    const navigation = useNavigation();
    const [showPass, setShowPass] = useState(true)
    const [showCPass, setShowCPass] = useState(true)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState('')
    const [error, setError] = useState("")
    const dispatch = useDispatch();

    const onBackPress = () => {
        navigation.goBack();
    }

    const EyeCPasswordPress = () => {
        setShowPass(!showPass)
    }

    const onSignupPress = () => {
        navigation.navigate('Signup')
    }

    const onCancelPress = () => {
        setError("")
    }

    const onLoginPress = async () => {
        if (email.trim() == "") {
            setError("Please enter email")
        } else if (!Config.regex.regExEmail.test(email)) {
            setError("Please enter valid email address")
        } else if (password.trim() == "") {
            setError("Please enter  password")
        } else if (password.length < 6) {
            setError("Please enter more than 5 characters")
        } else {
            const params = {
                "email": email.toLocaleLowerCase(),
                "password": password
            }
            const res: any = await dispatch(Login(params))
            if (res.code == 200) {
                if (res?.response?.isError) {
                    setError(res?.response?.msg)
                } else {
                    navigation.navigate('HomeScreen')
                }
            } else {

            }
            console.log("LoginRes===>", res);

        }
    }

    return (
        <View style={{ flex: 1 }}>
            <OnboardHeader isSkip={false} isBlueBackIcon={true} onBackPress={onBackPress} />
            <View style={styles.mainView}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <Text style={styles.title}>Wecome Back!</Text>
                    <Text style={styles.subTitle}>Thank you for get back to MySelf</Text>
                    <Text style={[styles.subTitle, { marginBottom: Config.size.scale(10) }]}>Login to continue.</Text>
                    <View style={{ marginVertical: Config.size.verticalScale(10) }}>
                        {error != "" && <ErrorPopup
                            errorText={error}
                            onCencalPress={onCancelPress}
                        />}
                        <TextInputComp
                            text={'Email address:'}
                            placeholder={"Enter your email address"}
                            onChangeText={(text) => setEmail(text)}
                        // is_icon={getIcons(Icons.EYE_ICON, 16)}
                        // secureTextEntry={showPass}
                        // onPress={EyePasswordPress}
                        />
                        <TextInputComp
                            text={'Password?'}
                            placeholder={"Enter Password"}
                            is_icon={showPass ? getIcons(Icons.EYE_ICON, 16) : getIcons(Icons.EYE_ICON_CLOSE, 16)}
                            secureTextEntry={showPass}
                            onPress={EyeCPasswordPress}
                            onChangeText={(text) => setPassword(text)}
                        />
                    </View>
                    <Button
                        text={"Login"}
                        onPress={onLoginPress}
                        style={{ marginTop: Config.size.verticalScale(10) }}
                    />
                    <View style={styles.signinView}>
                        <Text style={styles.textHeding}>
                            Don’t have an account?
                        </Text>
                        <TouchableOpacity
                            onPress={onSignupPress}
                            hitSlop={{ top: 12, bottom: 20, left: 50, right: 50 }}>
                            <Text style={styles.textSignin}>
                                Signup
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.middleView}>
                        <View style={styles.horizontalLine}></View>
                        <Text style={styles.middleText}>OR</Text>
                        <View style={styles.horizontalLine}></View>
                    </View>

                    <SocialLoginButton
                        text={"Sign up with Google"}
                        //onPress={onSignupPress}
                        style={{ marginVertical: Config.size.verticalScale(8) }}
                        icon={getIcons(Icons.GOOGLE_ICON, 20)}
                    />
                    <SocialLoginButton
                        text={"Sign up with Apple"}
                        //onPress={onSignupPress}
                        style={{ marginVertical: Config.size.verticalScale(8) }}
                        icon={getIcons(Icons.APPLE_ICON, 20)}
                    />
                    <SocialLoginButton
                        text={"Sign up with Facebook"}
                        //onPress={onSignupPress}
                        style={{ marginVertical: Config.size.verticalScale(8) }}
                        icon={getIcons(Icons.FACEBOOK_ICON, 20)}
                    />
                </ScrollView>
            </View>

        </View>

    )
}


const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // backgroundColor: Config.colors.backgroundColor,
        paddingHorizontal: Config.size.scale(15),
    },
    title: {
        fontSize: Config.size.scale(24),
        fontFamily: Config.fonts.MEDIUM
    },
    subTitle: {
        fontSize: Config.size.scale(14),
        color: Config.colors.textColorGray,
    },
    textSignin: {
        color: Config.colors.AppBlueColor,
        marginVertical: Config.size.verticalScale(10),
        // padding: Config.size.scale(2),
        fontFamily: Config.fonts.REGULAR,
        marginLeft: Config.size.scale(2)
    },
    signinView: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
    },
    horizontalLine: {
        flex: 1,
        height: Config.size.scale(1),
        backgroundColor: "#EDEDED",
    },
    middleText: {
        color: Config.colors.textColorGray,
        fontSize: Config.size.scale(16),
        fontWeight: 'bold',
        marginHorizontal: Config.size.scale(10)
    },
    middleView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: Config.size.verticalScale(10)
    },
    textHeding: {
        marginVertical: Config.size.verticalScale(10),
        fontFamily: Config.fonts.REGULAR
    }
})


export default HOC(LoginScreen)