import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import HOC from '../../HOC'
import Config from '../../../utils/Config'
import { normalize } from '../../../utils/Helpers'
import { OnboardHeader, Button, TextInputComp, SocialLoginButton, ErrorPopup } from '../../reuse'
import { getIcons, Icons } from '../../../assets/Icons'
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch, } from 'react-redux';
import { SignUpAction } from '../../../redux/actions/AuthActions'


const SignUpPasswordScreen = () => {
    const navigation = useNavigation();
    const [showPass, setShowPass] = useState(true)
    const [showCPass, setShowCPass] = useState(true)
    const [password, setPassword] = useState('')
    const [cPassword, setCPassword] = useState('')
    const [error, setError] = useState("")
    const routes = useRoute()
    const dispatch = useDispatch();

    const onBackPress = () => {
        navigation.goBack();
    }

    const EyePasswordPress = () => {
        setShowPass(!showPass)
    }
    const EyeCPasswordPress = () => {
        setShowCPass(!showCPass)
    }

    const onLoginPress = () => {
        navigation.navigate('LoginScreen')
    }

    const onCancelPress = () => {
        setError("")
    }

    const onSignUpPress = async () => {
        if (password.trim() == "") {
            setError("Please enter  password")
        } else if (password.length < 6) {
            setError("Please enter more than 5 characters")
        } else if (cPassword.trim() == "") {
            setError("Please enter confirm password")
        } else if (cPassword.length < 6) {
            setError("Please enter more than 5 characters")
        } else if (password !== cPassword) {
            setError("confirm password not match with password")
        } else {
            const params = {
                "name": {
                    "firstname": "Test",
                    "lastname": "User"
                },
                "phone": {
                    "code": "+91",
                    "number": routes.params?.data?.phone
                },
                "email": routes.params?.data?.email,
                "password": password
            }
            const res: any = await dispatch(SignUpAction(params))
            if (res.code == 200) {
                if (res?.response?.isError) {
                    setError(res?.response?.msg)
                } else {
                    navigation.navigate('HomeScreen')
                }
            } else {

            }
            console.log("signUp===>", res);

        }
    }


    return (
        <View style={{ flex: 1 }}>
            <OnboardHeader isSkip={false} isBlueBackIcon={true} onBackPress={onBackPress} />
            <View style={styles.mainView}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <Text style={styles.title}>Finalize</Text>
                    <Text style={styles.subTitle}>Secure your account with a personal  </Text>
                    <Text style={[styles.subTitle, { marginBottom: Config.size.scale(10) }]}>password.</Text>
                    <View style={{ marginVertical: Config.size.verticalScale(10) }}>
                        {error != "" && <ErrorPopup
                            errorText={error}
                            onCencalPress={onCancelPress}
                        />}
                        <TextInputComp
                            text={'Password?'}
                            placeholder={"Enter Password"}
                            value={password}
                            is_icon={showPass ? getIcons(Icons.EYE_ICON, 16) : getIcons(Icons.EYE_ICON_CLOSE, 16)}
                            secureTextEntry={showPass}
                            onPress={EyePasswordPress}
                            onChangeText={(text) => setPassword(text)}
                        />
                        <TextInputComp
                            text={'Confirm password?'}
                            placeholder={"Enter confirm Password"}
                            is_icon={showCPass ? getIcons(Icons.EYE_ICON, 16) : getIcons(Icons.EYE_ICON_CLOSE, 16)}
                            secureTextEntry={showCPass}
                            value={cPassword}
                            onPress={EyeCPasswordPress}
                            onChangeText={(text) => setCPassword(text)}
                        />
                    </View>
                    <Button
                        text={"Signup"}
                        onPress={onSignUpPress}
                        style={{ marginTop: Config.size.verticalScale(10) }}
                    />
                    <View style={styles.signinView}>
                        <Text style={styles.textHeding}>
                            Already have an account?
                        </Text>
                        <TouchableOpacity
                            onPress={onLoginPress}
                            hitSlop={{ top: 12, bottom: 20, left: 50, right: 50 }}>
                            <Text style={styles.textSignin}>
                                Login
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.middleView}>
                        <View style={styles.horizontalLine}></View>
                        <Text style={styles.middleText}>OR</Text>
                        <View style={styles.horizontalLine}></View>
                    </View>

                    <SocialLoginButton
                        text={"Sign up with Google"}
                        //onPress={onSignupPress}
                        style={{ marginVertical: Config.size.verticalScale(8) }}
                        icon={getIcons(Icons.GOOGLE_ICON, 20)}
                    />
                    <SocialLoginButton
                        text={"Sign up with Apple"}
                        //onPress={onSignupPress}
                        style={{ marginVertical: Config.size.verticalScale(8) }}
                        icon={getIcons(Icons.APPLE_ICON, 20)}
                    />
                    <SocialLoginButton
                        text={"Sign up with Facebook"}
                        //onPress={onSignupPress}
                        style={{ marginVertical: Config.size.verticalScale(8) }}
                        icon={getIcons(Icons.FACEBOOK_ICON, 20)}
                    />
                </ScrollView>
            </View>

        </View>

    )
}


const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // backgroundColor: Config.colors.backgroundColor,
        paddingHorizontal: Config.size.scale(15),
    },
    title: {
        fontSize: Config.size.scale(24),
        fontFamily: Config.fonts.MEDIUM
    },
    subTitle: {
        fontSize: Config.size.scale(14),
        color: Config.colors.textColorGray,
    },
    textSignin: {
        color: Config.colors.AppBlueColor,
        marginVertical: Config.size.verticalScale(10),
        // padding: Config.size.scale(2),
        fontFamily: Config.fonts.REGULAR,
        marginLeft: Config.size.scale(2)
    },
    signinView: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
    },
    horizontalLine: {
        flex: 1,
        height: Config.size.scale(1),
        backgroundColor: "#EDEDED",
    },
    middleText: {
        color: Config.colors.textColorGray,
        fontSize: Config.size.scale(16),
        fontWeight: 'bold',
        marginHorizontal: Config.size.scale(10)
    },
    middleView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: Config.size.verticalScale(10)
    },
    textHeding: {
        marginVertical: Config.size.verticalScale(10),
        fontFamily: Config.fonts.REGULAR
    }
})


export default HOC(SignUpPasswordScreen)