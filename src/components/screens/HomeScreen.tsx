import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import HOC from '../HOC'
import Config from '../../utils/Config'
import { normalize } from '../../utils/Helpers'
import { OnboardHeader, Button } from '../reuse'
import { useNavigation } from '@react-navigation/native';

const HomeScreen = () => {

    const navigation = useNavigation();

    const onSignupPress = () => {
        navigation.navigate('Signup')
    }
    const onLoginPress = () => {
        navigation.navigate('LoginScreen')
    }
    return (
        <View style={styles.mainView}>
            <View style={styles.logoView}>
                <Image
                    source={require('../../assets/Images/entryScreenLogo.png')}
                    style={styles.image}
                    resizeMode="contain"
                />
                {/* <View style={styles.buttonView}>
                    <Button
                        text={"Signup"}
                        onPress={onSignupPress}
                        style={{}}
                    />
                    <View style={styles.signinView}>
                        <Text>
                            Already have an account?
                        </Text>
                        <TouchableOpacity onPress={onLoginPress}>
                            <Text style={styles.textSignin}>
                                Sign in
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View> */}
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // backgroundColor: "red",
        paddingHorizontal: Config.size.scale(15),
    },
    image: {
        height: Config.size.verticalScale(100),
        width: Config.size.scale(200)
    },
    logoView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonView: {
        width: '100%',
        alignItems: 'center',
        position: 'absolute',
        bottom: Config.size.verticalScale(20)
    },
    textSignin: {
        color: Config.colors.AppBlueColor,
        marginVertical: Config.size.verticalScale(10),
        padding: Config.size.scale(2)
    },
    signinView: {
        flexDirection: 'row',
        alignItems: 'center'
    }

})


export default HOC(HomeScreen)