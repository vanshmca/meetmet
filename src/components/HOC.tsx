import React from 'react';
import { SafeAreaView, TouchableOpacity, Text, Keyboard, View, StyleSheet, ScrollView } from 'react-native';
import { useSelector } from 'react-redux';
import LoadingComp from '../components/reuse/LoadingComp';
import { Log } from '../utils/Logger';
import { RootState } from '../redux/reducers';
import { getIcons, Icons } from '../assets/Icons';
import { moderateScale } from 'react-native-size-matters';
import Config from '../utils/Config'

/**
 * HOC for including reusable UI logic
 */

const HOC = (ChildComponent: React.FC, left_Icon_Name?, riight_Icon_Name?): React.FC => {
    function InnerHOC(props: any) {
        const loadingStatus = useSelector(
            (state: RootState) => state.LoadingReducer.loadingStatus,
        );

        props.navigation.setOptions({
            headerShown: false,
            headerStyle: {
                backgroundColor: '#fff',
                shadowColor: '#fff',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
                fontSize: 30,
                fontStyle: Config.fonts.SEMI_BOLD
            },


        });
        return (
            <SafeAreaView
                style={{ flex: 1, backgroundColor: "#F8F8F8" }}>
                {/* <ScrollView
                    containerStyle={{ flex: 1 }}
                    style={{ height: '100%', backgroundColor: "#F8F8F8" }} showsVerticalScrollIndicator={false}> */}
                <ChildComponent />
                {/* </ScrollView> */}
                {loadingStatus ? <LoadingComp /> : null}
            </SafeAreaView>
        );
    }
    return InnerHOC;
};

const styles = StyleSheet.create({
    topIconDisplay: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    backButtonStyle: {
        marginHorizontal: 20,
        padding: moderateScale(10),
        paddingLeft: 0,
    },
})

export default HOC;
